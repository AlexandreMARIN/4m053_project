import matplotlib.pyplot as plt
import json
import numpy as np

##############################
# this script displays norms
# of the residual vectors
# for two sorts of sparse
# matrices
# and for both methods
# (conjugate gradient with
# and without preconditioning)
##############################

#we open a file and load its data
datafile = open("./json/resvec2.json")
data = json.load(datafile)

#here we plot the figure
names = ["laplacian", "H"]
for n in names:
    plt.plot(data[n]["resvec_cg"], "--", data[n]["resvec_pcg"], "-+")

plt.yscale('log')
leg = ["laplacian("+str(data["dim"])+")", "H("+str(data["dim"])+")"]
plt.legend([leg[0]+", cg()", leg[0]+", pcg()", leg[1]+", cg()", leg[1]+", pcg()"])
plt.title(r"Historiques de convergence (matrices creuses, tolérance : "+str(data["epsilon"])+")")
plt.xlabel("itération", fontsize='large')
plt.ylabel(r"$||r_{k}||_{"+data["norm"]+"}/||b||_{"+data["norm"]+"}$", fontsize='x-large')
plt.show()
