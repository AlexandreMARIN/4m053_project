#include <Block.hpp>
#include <fstream>
#include <string>

using namespace std;

/*here is the answer to the question 9*/

int main(int argc, char* argv[]){

  /*these numbers refer to the matrices in supplied files*/
  vector<string> numbers{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15"};

  DenseMatrix A(0, 0);
  double epsilon;
  int n_max;
  IterSolver::Norm norm;

  if(argc!=4){
    cout << "\n3 arguments are expected\n\t./bin/compare epsilon n_max norm_type\nexample:\n\t./bin/compare 0.1 20 0\n";
    return 1;
  }

  //arguments given to main()
  string arg1(argv[1]);
  epsilon = stod(arg1);
  string arg2(argv[2]);
  n_max = stod(arg2);
  string arg3(argv[3]);
  norm = static_cast<IterSolver::Norm>(stoi(arg3));
  ConjGrad_.set_norm(norm);
  ASM_.set_norm(norm);

  //this file will contain results
  ofstream file("./json/resvec.json");

  file << "{\n";
  file << "\t\"epsilon\" : " << epsilon << ",\n";
  file << "\t\"norm\" : ";
  switch(norm){
  case IterSolver::Norm::INFTY :
    file << "\"\\\\infty\"";
    break;
  case IterSolver::Norm::EUCLIDIAN:
    file << "\"2\"";
    break;
  case IterSolver::Norm::NORM_A:
    file << "\"A\"";
    break;
  }
  file << ",\n";
  for(string num : numbers){
    A.LoadFromFile("./matrices/matrix"+num+".txt");
    file << "\t\"" << num << "\" : {\n" << A.resvec_to_json(epsilon, n_max) << "\n\t},\n";
  }
  file.seekp(-2, ios_base::cur);
  file << "\n}";

  return 0;
}
