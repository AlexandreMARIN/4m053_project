import matplotlib.pyplot as plt
import json
import numpy as np

##############################
# this script displays norms
# of the residual vectors
# for each supplied matrix
# and for both methods
# (conjugate gradient with
# and without preconditioning)
##############################

#we open a file and load data
datafile = open("./json/resvec.json")
data = json.load(datafile)

#we plot five figures (6 curves per figure)
for p in range(5):
    num = [str(i) for i in range(1+3*p, 1+3*(p+1))]

    for n in num:
        plt.plot(data[n]["resvec_cg"], "--", data[n]["resvec_pcg"], "-+")

    plt.yscale('log')

    names = []
    for n in num:
        names.append(n+" : cg()")
        names.append(n+" : pcg()")

    plt.legend(names)
    plt.title(r"Historiques de convergence (matrices de taille 100, tolérance : "+str(data["epsilon"])+")")
    plt.xlabel("itération", fontsize='large')
    plt.ylabel(r"$||r_{k}||_{"+data["norm"]+"}/||b||_{"+data["norm"]+"}$", fontsize='x-large')
    plt.show()
