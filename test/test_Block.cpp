#include <Block.hpp>

using namespace std;

int main(){

  /* we are going to build integer vectors */

  int l, r, n;
  cout << "Give three positive integers :\nl = ";
  cin >> l;
  cout << "r = ";
  cin >> r;
  cout << "n = ";
  cin >> n;

  list<vector<int> > lvi = getListFromI_n(l, r, n);

  cout << "Results for getListFromI_n(l, r, n) :\n";

  cout << "[\n";
  for(const vector<int>& v : lvi){
    cout << v << "\n";
  }
  cout << "]\n\n";


  /* we are going to build a BlockMatrix object from a DenseMatrix object */
  DenseMatrix A(n, n);
  A.diag(2.0);
  A.diag(-1.0, 1);
  A.diag(-1.0, -1);

  BlockMatrix B(n, n);
  B.Extract(A);

  cout << "\n----------\nHere is a BlockMatrix:\n\nB :=\n" << B;

  return 0;
}
