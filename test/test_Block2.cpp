#include <Block.hpp>

using namespace std;

int main(){

  /*
    We test:
    - the operator += for BlockMatrix ;
    - the method MinRes ;
    - the method MvProd() from BlockMatrix.
  */

  int n;
  cout << "Give an integer greater than 3 : ";
  cin >> n;

  CSR L(CSR::laplacian(n));
  DenseMatrix L2(n, n);
  L2.diag(2.0);
  L2.diag(-1.0, 1);
  L2.diag(-1.0, -1);
  BlockMatrix A(n, n);

  vector<int> I1{0, n-1}, I2{(n/2)-1, n/2, (n/2)+1}, I3(n/2), I4(n/2);
  for(int i=0;2*i<n;i++){
    I3[i] = 2*i;
    I4[i] = 2*i+1;
  }

  Block B1(n, n, I1, I1, new CSR{L(I1, I1)});
  Block B2(n, n, I2, I2, new DenseMatrix(L2(I2, I2)));
  Block B3(n, n, I3, I3, new CSR{L(I3, I3)});
  Block B4(n, n, I4, I4, new CSR{L(I4, I4)});

  A += B1;
  A += B2;
  A += B3;
  A += B4;
  cout << "A:=\n" << A;

  vector<double> b(n), x;
  b.assign(n, 1.0);
  MinRes_.set_tol(1e-3);
  MinRes_.set_n_max(n);
  A.MinRes(x, b);
  cout << "\nb = " << b << "\nFor the system Ax=b,\nx = " << x;
  A.MvProd(x, b);
  cout << "\nAx = " << b << "\n";

  return 0;
}
