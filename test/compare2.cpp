#include <Block.hpp>
#include <fstream>
#include <string>

using namespace std;

/*here is the answer to the question 9, for sparse matrices*/

int main(int argc, char* argv[]){

  CSR A(0, 0);
  double epsilon;
  int n_max;
  int dim;//size of the matrices
  IterSolver::Norm norm;

  if(argc!=5){
    cout << "\n4 arguments are expected\n\t./bin/compare2 epsilon n_max norm_type dim\nexample:\n\t./bin/compare 0.01 20 1 200\n";
    return 1;
  }

  //arguments given to main()
  string arg1(argv[1]);
  epsilon = stod(arg1);
  string arg2(argv[2]);
  n_max = stod(arg2);
  string arg3(argv[3]);
  norm = static_cast<IterSolver::Norm>(stoi(arg3));
  string arg4(argv[4]);
  dim = stod(arg4);

  ConjGrad_.set_norm(norm);
  ASM_.set_norm(norm);

  //that file will contain results
  ofstream file("./json/resvec2.json");

  file << "{\n";
  file << "\t\"epsilon\" : " << epsilon << ",\n";
  file << "\t\"dim\" : " << dim << ",\n";
  file << "\t\"norm\" : ";
  switch(norm){
  case IterSolver::Norm::INFTY :
    file << "\"\\\\infty\"";
    break;
  case IterSolver::Norm::EUCLIDIAN:
    file << "\"2\"";
    break;
  case IterSolver::Norm::NORM_A:
    file << "\"A\"";
    break;
  }
  file << ",\n";

  A = CSR::laplacian(dim);
  file << "\t\"laplacian\" : {\n" << A.resvec_to_json(epsilon, n_max) << "\n\t},\n";
  A = CSR::H(dim);
  file << "\t\"H\" : {\n" << A.resvec_to_json(epsilon, n_max) << "\n\t}";
  file << "\n}";

  return 0;
}
