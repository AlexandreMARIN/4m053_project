#include <iterative_solvers.hpp>

using namespace std;

int main(){

  int r, c;

  cout << "Give two integers,\nnr = ";
  cin >> r;

  cout << "nc = ";
  cin >> c;

  /* we test the constructor and the operator << */
  DenseMatrix A(r, c);
  cout << "A :=\n" << A;

  /* we test assignment and the method MvPord() */
  DenseMatrix B(2, 2);
  B(0, 0) = -1.0;
  B(1, 0) = 2.0;
  B(0, 1) = 3.0;
  B(1, 1) = .5;

  cout << "\n---------------\nB :=\n" << B;

  vector<double> v{1.0, -0.5}, w(2);
  cout << "v := " << v << "\n";

  B.MvProd(v, w);
  cout << "Bv = " << w;


  /* we test the methods cg() and LUSolve() */
  cout << "\n----------\nGive a positive integer\nn = ";
  int n;
  cin >> n;
  DenseMatrix C(n, n);
  C.diag(2.0);
  C.diag(-1., 1);
  C.diag(-1., -1);

  cout << "C:=\n" << C;
  vector<double> b(n, 1.0), x(n);
  cout << "b:= " << b;

  C.LUSolve(x, b);
  cout << "\n\tVia LUSolve():\nFor the system Cx = b, \nx = " << x;

  ConjGrad_.set_tol(0.01);
  ConjGrad_.set_n_max(n);
  C.cg(x, b);
  cout << "\n\tVia cg():\nFor the system Cx = b, \nx = " << x << "\nnumber of iterations : " << ConjGrad_.get_niter() << "\nnorms of the residual vectors :\n" << ConjGrad_.get_resvec() <<"\n";


  /* we test submatrix extractions */
  vector<int> Ir, Ic;
  int p, q;
  DenseMatrix D(r, c);
  D.diag(2.0);
  D.diag(-3.0, 1);
  D.diag(.5, -2);
  D(r/2, (c*2)/3) = 1.5;
  cout << "\n-----------------\nD:=\n" << D;

  cout << "Give an integer less than " << r << "\np = ";
  cin >> p;
  Ir.resize(p);
  cout << "Give an integer less than " << c << "\nq = ";
  cin >> q;
  Ic.resize(q);

  cout << "\nGive " << p << " ordered integers between 0 and " << r-1 << ":\n";
  for(int i=0;i<p;i++){
    cout << "\nIr(" << i << ") = ";
    cin >> Ir[i];
  }

  cout << "\nGive " << q << " ordered integers between 0 and " << c-1 << ":\n";
  for(int i=0;i<q;i++){
    cout << "\nIc(" << i << ") = ";
    cin >> Ic[i];
  }

  cout << "\nD(Ir, Ic) =\n" << D(Ir, Ic);


  return 0;
}
