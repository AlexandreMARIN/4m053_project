#include <iterative_solvers.hpp>

using namespace std;

int main(){

  int r, c;
  cout << "Give two positive integers: ";
  cout << "\nr = ";
  cin >> r;
  cout << "\nc = ";
  cin >> c;

  DenseMatrix A(r, c);
  A.diag(2.0);
  A.diag(-1.0, 1);
  A(r/2, (2*c)/3) = 3.5;
  A((3*r)/4, c/3) = -0.6;

  cout << "A:=\n" << A;

  /*we build a sparse matrix from a dense matrix*/
  CSR B(A);
  cout << "B:=\n" << B;

  /*we test extraction of submatrices here*/
  vector<int> Ir, Ic;
  int p, q;

  cout << "Give an integer less than " << r << "\np = ";
  cin >> p;
  Ir.resize(p);
  cout << "Give an integer less than " << c << "\nq = ";
  cin >> q;
  Ic.resize(q);

  cout << "\nGive " << p << " ordered integers between 0 and " << r-1 << ":\n";
  for(int i=0;i<p;i++){
    cout << "\nIr(" << i << ") = ";
    cin >> Ir[i];
  }

  cout << "\nGive " << q << " ordered integers between 0 and " << c-1 << ":\n";
  for(int i=0;i<q;i++){
    cout << "\nIc(" << i << ") = ";
    cin >> Ic[i];
  }

  CSR B2(B(Ir, Ic));
  cout << "\nB(Ir, Ic) =\n" << B2 << "\n=\n" << (DenseMatrix)B2;


  /*
    We test:
    - the operator () for CSR ;
    - the conjugate gradient ;
    - the method MvProd() ;
    - the method LUSolve() ;
    - the cast operator towards DenseMatrix.

    We check the output of two static methods of CSR:
    - laplacian() ;
    - H().
  */
  CSR C(CSR::laplacian(r));
  cout << "\nC:=laplacian(" << r << "):\n" << C;
  vector<double> x, b(r);
  b.assign(r, 1.0);
  cout << "\nb:=" << b;
  ConjGrad_.set_n_max(r);
  C.cg(x, b);
  cout << "\nFor Cx=b, via cg()\nx=" << x << "\n";
  C.LUSolve(x, b);
  cout << "\nFor Cx=b, via LUSolve()\nx=" << x << "\n";
  C.MvProd(x, b);
  cout << "\nCx = " << b;

  const CSR H(CSR::H(r));
  cout << "H(" << r << ")=\n" << H << "\n=\n" << (DenseMatrix)H;

  cout << "Give tow integers in [0, " << r-1 << "] : " ;
  int i, j;
  cin >> i >> j;
  cout << "\nH(" << i << ", " << j << ") = " << H(i, j);

  return 0;
}
