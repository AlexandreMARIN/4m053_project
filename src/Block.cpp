#include "../include/Block.hpp"

#include <cassert>
#include <iomanip>
#include <random>
#include <typeinfo>
#include <chrono>
#include <stdexcept>

using namespace std;
using namespace std::chrono;


/**************Class Block******************/
Block::Block(const Block& B) : Matrix(B.nr, B.nc), Ir(B.Ir), Ic(B.Ic), mat(B.mat->clone()){}

Block::Block(Block&& B) : Matrix(B.nr, B.nc), Ir(B.Ir), Ic(B.Ic), mat(B.mat){
  B.mat = nullptr;
}

Block::Block(int r, int c, vector<int> I, vector<int> J, AtomicMatrix* ptr) : Matrix(r, c), Ir(I), Ic(J), mat(ptr?ptr:new DenseMatrix{(int)I.size(), (int)J.size()}){}

Block::~Block(){
  delete mat;
}


Block& Block::operator=(const Block& B){
  nr = B.nr;
  nc = B.nc;
  Ir = B.Ir;
  Ic = B.Ic;
  mat = B.mat->clone();

  return *this;
}

Block& Block::operator=(Block&& B){
  nr = B.nr;
  nc = B.nc;
  Ir = B.Ir;
  Ic = B.Ic;
  mat = B.mat;
  B.mat = nullptr;

  return *this;
}

void Block::MvProd(const vector<double>& x, vector<double>& b) const{

  int k, l;
  const AtomicMatrix& B = *mat;

  assert((vector<double>::size_type)nc==x.size());

  b.resize(0);
  b.reserve(nr);

  for(k=0;k<Ir[0];k++){
    b.push_back(0.0);
  }

  for(l=0;(vector<int>::size_type)(l+1)<Ir.size();l++){
    for(k=Ir[l];k<Ir[l+1];k++){
      b.push_back(0.0);
    }
    for(k=0;(vector<int>::size_type)k<Ic.size();k++){
      b[Ir[l]] += B(l, k)*x[Ic[k]];
    }
  }

  //l's value is |Ir|-1
  for(k=l;(vector<double>::size_type)k<x.size();k++){
    b.push_back(0.0);
  }
  for(k=0;(vector<int>::size_type)k<Ic.size();k++){
    b[Ir[l]] += B(l, k)*x[Ic[k]];
  }


}

void Block::MvProdInv(const vector<double>& x, vector<double>& b) const{

  assert(Ir.size() == Ic.size() && (vector<double>::size_type)nc==x.size());//the matrix pointed to by mat must be invertible

  //we compute y:=(R_(Ic)^T)x
  vector<double> y;
  int i, j;
  y.reserve(Ic.size());
  for(i=0;(vector<int>::size_type)i<Ic.size();i++){
    y.push_back(x[Ic[i]]);
  }


  //then we solve Bz=y, where B is the matrix pointed to by mat
  vector<double> z;
  mat->cg(z, y);

  //we assign (R_(Ir))z to b
  b.reserve(nr);
  b.resize(0);
  for(i=0;i<Ir[0];i++){
    b.push_back(0.0);
  }
  for(j=0;(vector<int>::size_type)(j+1)<Ir.size();j++){
    b.push_back(z[j]);
    for(i=Ir[j]+1;i<Ir[j+1];i++){
      b.push_back(0.0);
    }
  }
  b.push_back(z[j]);
  for(i=Ir.back()+1;i<nr;i++){
    b.push_back(0.0);
  }


}

Block* Block::clone() const{
  return new Block(*this);
}

ostream& operator<<(ostream& os, const Block& B){
  os << "Ir := " << B.Ir << "\nIc := " << B.Ic << "\nmat : \n";
  if(typeid(DenseMatrix)==typeid(*B.mat)){
    os << *dynamic_cast<DenseMatrix*>(B.mat);
    return os;
  }
  if(typeid(CSR)==typeid(*B.mat)){
    os << *dynamic_cast<CSR*>(B.mat);
  }
  return os;
}


/***************Class BlockMatrix*****************/
BlockMatrix::BlockMatrix(int r, int c) : Matrix{r, c}, val{}{}

BlockMatrix& BlockMatrix::operator+=(const Block& B){
  val.push_back(B);
  return *this;
}



void BlockMatrix::MvProd(const vector<double>& x, vector<double>& b) const{
  assert((vector<double>::size_type)nc==x.size());

  b.resize(nr);
  vector<double> aux(nr);
  int i;
  b.assign(nr, 0.0);
  for(const Block& B : val){
    B.MvProd(x, aux);
    for(i=0;i<nr;i++){
      b[i] += aux[i];
    }
  }

}

void BlockMatrix::MvProdInv(const vector<double>& x, vector<double>& b) const{

  vector<double> aux;
  int i;


  if(!val.empty()){
    val.front().MvProdInv(x, b);
  }else{
    b.reserve(nr);
    b.assign(nr, 0.0);
    return;
  }
  list<Block>::const_iterator it;
  for(it=val.cbegin(), it++;it!=val.cend();it++){
    it->MvProdInv(x, aux);
    for(i=0;i<nr;i++){
      b[i] += aux[i];
    }
  }

}

void BlockMatrix::MinRes(vector<double>& x, const vector<double>& b) const{

  assert(nr==nc && (vector<double>::size_type)nr==b.size());

  MinRes_.set_A(this);
  MinRes_.set_b(&b);
  MinRes_.solve();
  x = MinRes_.get_x();

}

void BlockMatrix::Extract(const AtomicMatrix& A){
  assert(nr==nc);

  /*
    getListFromI_n(l, r, n)
    In order to have correct values, we must have:
    - l>0, r>=0
    - l+r<=n
    - [n/l]>=2
    - l-r>0
    - l([n/l]-2)+r<=n

    We want these tuples to cover the set [1, nr], so we must have:
    - n = nr
  */
  high_resolution_clock::time_point tp = high_resolution_clock::now();
  linear_congruential_engine<long unsigned int, 16807ul, 0ul, 2147483647ul> lce{static_cast<linear_congruential_engine<long unsigned int, 16807ul, 0ul, 2147483647ul>::result_type>(tp.time_since_epoch().count())};
  uniform_int_distribution<int> uid{1, nr/3};
  int l, r, rmax;

  l = uid(lce);

  rmax = nr-l*((nr/l)-2);
  //r<= min(nr-l, l-1, nr-l([nr/l]-2))
  if(rmax>nr-l){
    rmax = nr-l;
  }
  if(rmax>=l){
    rmax = l-1;
  }
  uniform_int_distribution<int> uid2{0, rmax};
  r = uid2(lce);

  list<vector<int> > lvi(getListFromI_n(l, r, nr));
  AtomicMatrix* ptr;

  if(typeid(A)==typeid(DenseMatrix)){
    for(const vector<int>& v : lvi){
      ptr = new DenseMatrix{move((dynamic_cast<const DenseMatrix&>(A))(v, v))};
      val.push_back(Block{nr, nc, v, v, ptr});
    }
    return;
  }

  if(typeid(A)==typeid(CSR)){
    for(const vector<int>& v : lvi){
      ptr = new CSR{move((dynamic_cast<const CSR&>(A))(v, v))};
      val.push_back(Block{nr, nc, v, v, ptr});
    }
    return;
  }

  throw(invalid_argument("void BlockMatrix::Extract(const AtomicMatrix& A) : A must be either a DenseMatrix or a CSR !\n"));
}

BlockMatrix* BlockMatrix::clone() const{
  return new BlockMatrix(*this);
}

ostream& operator<<(ostream& os, const BlockMatrix& Bm){
  os << "[\n";
  for(const Block& B : Bm.val){
    os << B << "\n----------------\n";
  }
  os << "]";
  return os;
}



ostream& operator<<(ostream& os, const vector<int>& v){
  os << "( ";
  if(v.size()){
    os << setw(5) << v[0];
  }
  for(vector<int>::const_iterator it=v.begin()+1;it!=v.end();it++){
    os << ", " << setw(5) << *it;
  }
  os << " )";
  return os;
}



list<vector<int> > getListFromI_n(int l, int r, int n){
  assert( l>0 && r>=0 && n>=0 && n>=l+r && n>=2*l && l>r && l*((n/l)-2)+r<=n );

  vector<int> var;
  list<vector<int> > lvi;
  int i, q, q_star = (n/l)-1;

  var.resize(l+r);
  for(i=0;i<l+r;i++){
    var[i] = i;
  }
  lvi.push_back(var);

  if(q_star>=2){
    var.resize(l+2*r+1);
    var[0] = l-r-1;
    for(i=1;(vector<int>::size_type)i<var.size();i++){
      var[i] = var[i-1]+1;
    }
    lvi.push_back(var);
    for(q=3;q<=q_star;q++){
      for(i=0;(vector<int>::size_type)i<var.size();i++){
	var[i] += l;
      }
      lvi.push_back(var);
    }
  }

  //I_(q_star+1)
  var.resize(n-l*q_star+1);
  var[0] = l*q_star - 1;
  for(i=1;(vector<int>::size_type)i<var.size();i++){
    var[i] = var[i-1]+1;
  }
  lvi.push_back(var);

  return lvi;
}


/***************Class ASM*****************/
ASM ASM::obj;

void ASM::init(){
  P_ = BlockMatrix{A_->get_nr(), A_->get_nc()};
  if(typeid(*A_)==typeid(DenseMatrix) || typeid(*A_)==typeid(CSR)){
    P_.Extract(*dynamic_cast<const AtomicMatrix*>(A_));
  }else{
    throw(invalid_argument("ASM::init() : A_ does not point to an AtomicMatrix !\n"));
  }
  P_.MvProdInv(*b_, z_);
  p_ = z_;
  aux.resize(p_.size());
}

void ASM::update_solution(){
  gamma_ = 0.0;
  int i;
  for(i=0;(vector<double>::size_type)i<r_.size();i++){
    gamma_ += z_[i]*r_[i];
  }
  alpha_ = gamma_/sq_norm(p_, Norm::NORM_A);
  for(i=0;(vector<double>::size_type)i<p_.size();i++){
    x_[i] += alpha_*p_[i];
  }
}

void ASM::update_resvec(){

  A_->MvProd(p_, aux);
  int i;
  for(i=0;(vector<double>::size_type)i<r_.size();i++){
    r_[i] -= alpha_*aux[i];
  }
  P_.MvProdInv(r_, z_);
  alpha_ = 0.0;
  for(i=0;(vector<double>::size_type)i<r_.size();i++){
    alpha_ += z_[i]*r_[i];
  }
  alpha_ /= gamma_;
  for(i=0;(vector<double>::size_type)i<r_.size();i++){
    p_[i] = z_[i] + alpha_*p_[i];
  }

}

ASM& ASM::getobj(){
  return obj;
}

ASM& ASM_ = ASM::getobj();
