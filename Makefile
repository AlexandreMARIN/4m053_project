INCLDIR	:= include
OBJDIR	:= obj
SRCDIR	:= src
BINDIR	:= bin
TESTDIR := test

CXX     := g++
CXXFLAGS  := -std=c++11 -Wall -I $(INCLDIR)


SRCS = $(wildcard $(SRCDIR)/*.cpp)
OBJS = $(subst $(SRCDIR)/,$(OBJDIR)/, $(subst .cpp,.o, $(SRCS)))


EXEC = test_Matrix test_Block compare test_CSR compare2 test_Block2

all : makedir $(EXEC)


$(EXEC) : %: $(TESTDIR)/%.cpp $(OBJS)
	$(CXX) $(CXXFLAGS) -o $(BINDIR)/$@ $^

$(OBJDIR)/%.o : $(SRCDIR)/%.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<


clean :
	rm -rf $(OBJDIR)/*
	rm -rf $(BINDIR)/*
	rm -rf *~

makedir :
	mkdir -p $(BINDIR)
	mkdir -p $(OBJDIR)
	mkdir -p json

print :
	@clear
	@echo "sources :\n"
	-@ls $(SRCDIR)
	@echo "objects :\n"
	-@ls $(OBJDIR)
	@echo "header files :\n"
	-@ls $(INCLDIR)
	@echo "contents of /test :\n"
	-@ls $(TESTDIR)

.PHONY: all clean makedir print
#Remarks:
# $@ : filename representing the target
# $< : filename of the first prerequisite
# $^ : filenames of all prerequisites, separated by spaces. Dupplicated are removed.
# $? : names of all prerequisites that are newer than the target, separated by spaces
# $+ : similar to $^ but include dupplicates
# $* : stem of target file (filename without suffix)
