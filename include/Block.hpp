#ifndef BLOCK_HPP
#define BLOCK_HPP

#include <list>

#include "iterative_solvers.hpp"


/***************Class Block*****************/
class Block : public Matrix{


  std::vector<int> Ir, Ic;
  AtomicMatrix* mat;

public:

  Block() = default;
  Block(const Block&);
  Block(Block&&);
  Block(int, int, std::vector<int>, std::vector<int>, AtomicMatrix* = nullptr);
  ~Block();

  Block& operator=(const Block&);
  Block& operator=(Block&&);


  void MvProd(const std::vector<double>&, std::vector<double>&) const;
  void MvProdInv(const std::vector<double>&, std::vector<double>&) const;

  Block* clone() const;
  friend std::ostream& operator<<(std::ostream&, const Block&);
};


/**************Class BlockMatrix******************/
class BlockMatrix : public Matrix{


  std::list<Block> val;

public:

  BlockMatrix() = default;
  BlockMatrix(const BlockMatrix&) = default;
  BlockMatrix(BlockMatrix&&) = default;
  BlockMatrix(int, int);
  ~BlockMatrix() = default;

  BlockMatrix& operator=(const BlockMatrix&) = default;
  BlockMatrix& operator=(BlockMatrix&&) = default;
  BlockMatrix& operator+=(const Block&);

  void MvProd(const std::vector<double>&, std::vector<double>&) const;
  void MvProdInv(const std::vector<double>&, std::vector<double>&) const;
  void MinRes(std::vector<double>&, const std::vector<double>&) const;

  void Extract(const AtomicMatrix&);

  BlockMatrix* clone() const;
  friend std::ostream& operator<<(std::ostream&, const BlockMatrix&);
};


std::ostream& operator<<(std::ostream&, const std::vector<int>&);
/*
  That function displays a vector composed of integers
*/

std::list<std::vector<int> > getListFromI_n(int, int, int);
/*
  That function builds the family defined in question 4)
*/


/****************Additive Schwarz Method****************/
class ASM final: public IterSolver{

  //for computing
  BlockMatrix P_;
  double alpha_, gamma_;
  std::vector<double> aux, p_, z_;

  static ASM obj;

  ASM() = default;
  ASM(const ASM&) = delete;
  ASM(ASM&&) = delete;
  ~ASM() = default;
  ASM& operator=(const ASM&) = delete;
  ASM& operator=(ASM&&) = delete;

  void init() override;
  void update_solution() override;
  void update_resvec() override;

public:

  static ASM& getobj();

};

extern ASM& ASM_;

#endif
